﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


namespace WebApplication2
{
    /// <summary>
    /// Opis podsumowujący dla WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Aby zezwalać na wywoływanie tej usługi sieci Web ze skryptu za pomocą kodu ASP.NET AJAX, usuń znaczniki komentarza z następującego wiersza. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Witaj świecie";
        }
        [WebMethod]
        public string Test(string login, string password)
        {
            utils.Player player = new utils.Player();
            utils.dbAdaptor db = new utils.dbAdaptor();

            player = db.Login(login, password);
            string str = "Gamer_no: " + player.GamerNo.ToString() + " Crew: " + player.Crew.ToString() + " Canons: " + player.Canons.ToString() +" Ammo:" +
                " " + player.Ammo.ToString() + " Gold: " + player.Gold.ToString() + " Health: " + player.Health.ToString() + " Name: " + player.ShipName;  
            return str;
        }
        [WebMethod]
        public utils.Player Login(string login, string password)
        {
            utils.Player player = new utils.Player();
            utils.dbAdaptor db = new utils.dbAdaptor();

            player = db.Login(login, password);
            return player;
        }
        [WebMethod]
        public List<utils.Player> LookAround(float lat, float lng)
        {            
            utils.dbAdaptor db = new utils.dbAdaptor();
            return db.LookAround(lat, lng);

        }
        [WebMethod]
        public List<utils.Player> Fight(int g1, int g2)
        {
            utils.dbAdaptor db = new utils.dbAdaptor();
            return db.Fight(g1, g2);
        }
        [WebMethod]
        public List<utils.Player> Fire(int g1, int g2)
        {
            utils.dbAdaptor db = new utils.dbAdaptor();
            return db.Fire(g1, g2);
        }
    }
}
