﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.utils
{
    public class Player
    {
        public int GamerNo {get; set;}
        public int Crew { get; set; }
        public int Canons { get; set; }
        public int Ammo { get; set; }
        public int Gold { get; set; }
        public int Health { get; set; }
        public int Status { get; set; }
        public string ShipName { get; set; }
    }
}