﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace WebApplication2.utils
{
    public class dbAdaptor
    {
        string con = "Host=localhost;Username=postgres;Password=postgres;Database=postgres";

        public void connect()
        {
            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM sls";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string trolo = reader.GetString(0);
                        }
                    }
                }
            }
        }
        public Player Login(string login, string password)
        {
            Player player = new Player();

            int gamerNo = 0;

            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT gamer_no FROM sls WHERE login=@login AND passwd=@passwd AND active=true ";

                    cmd.Parameters.AddWithValue("@login", login);
                    cmd.Parameters.AddWithValue("@passwd", password);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            gamerNo = reader.GetInt32(0);
                        }
                    }
                }
                if (gamerNo > 0)
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT crew, canons, ammo, gold, health, name FROM ships WHERE gamer_no = @gamerNo";

                        cmd.Parameters.AddWithValue("@gamerNo", gamerNo);
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                player.Crew = reader.GetInt32(0);
                                player.Canons = reader.GetInt32(1);
                                player.Ammo = reader.GetInt32(2);
                                player.Gold = reader.GetInt32(3);
                                player.Health = reader.GetInt32(4);
                                player.ShipName = reader.GetString(5);
                            }
                        }
                    }
                }
            }
            player.GamerNo = gamerNo;
            return player;
        }
        public List<Player> LookAround(float lat, float lng)
        {
            var PlayerList = new List<Player>();

            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT name, gamer_no FROM ships WHERE lat BETWEEN @min_lat AND @max_lat AND lng BETWEEN @min_lng AND @max_lng ";

                    cmd.Parameters.AddWithValue("@min_lat", lat - 0.0001);
                    cmd.Parameters.AddWithValue("@max_lat", lat + 0.0001);
                    cmd.Parameters.AddWithValue("@min_lng", lng - 0.0001);
                    cmd.Parameters.AddWithValue("@max_lng", lng + 0.0001);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Player p = new Player();
                            p.ShipName = reader.GetString(0);
                            p.GamerNo = reader.GetInt32(1);
                            PlayerList.Add(p);

                        }
                    }
                }
            }
            return PlayerList;
        }
        public List<Player> Fight(int g1, int g2)
        {
            List <Player> fightList = new List<Player>();
            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT gamer_no, crew, canons, gold, health, ammo, name FROM ships WHERE gamer_no IN (@g1 , @g2) ";

                    cmd.Parameters.AddWithValue("@g1", g1);
                    cmd.Parameters.AddWithValue("@g2", g2);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Player p = new Player();                            
                            p.GamerNo = reader.GetInt32(0);
                            p.Crew = reader.GetInt32(1);
                            p.Canons = reader.GetInt32(2);
                            p.Gold = reader.GetInt32(3);
                            p.Health = reader.GetInt32(4);
                            p.Ammo = reader.GetInt32(5);
                            p.ShipName = reader.GetString(6);
                            fightList.Add(p);
                        }
                    }
                }
            }
            return fightList;
        }
        public List<Player> Fire(int g1, int g2)
        {
            List<Player> fightList = Fight(g1, g2);
            CalucaleOutcome(fightList);

            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE public.ships SET crew =@crew1, ammo =@ammo1, health =@health1 WHERE gamer_no = @g1; " +
                        "UPDATE public.ships SET crew =@crew2, ammo =@ammo2, health =@health2 WHERE gamer_no = @g2; ";

                    cmd.Parameters.AddWithValue("@crew1", fightList[0].Crew);
                    cmd.Parameters.AddWithValue("@ammo1", fightList[0].Ammo);
                    cmd.Parameters.AddWithValue("@health1", fightList[0].Health);
                    cmd.Parameters.AddWithValue("@g1", fightList[0].GamerNo);
                    cmd.Parameters.AddWithValue("@crew2", fightList[1].Crew);
                    cmd.Parameters.AddWithValue("@ammo2", fightList[1].Ammo);
                    cmd.Parameters.AddWithValue("@health2", fightList[1].Health);
                    cmd.Parameters.AddWithValue("@g2", fightList[1].GamerNo);

                    cmd.ExecuteNonQuery();
                }
            }

            using (var conn = new NpgsqlConnection(con))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT gamer_no, crew, canons, gold, health, ammo, lat, long, name FROM ships WHERE gamer_no IN (@g1 , @g2) ";

                    cmd.Parameters.AddWithValue("@g1", fightList[0].GamerNo);
                    cmd.Parameters.AddWithValue("@g2", fightList[1].GamerNo);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Player p = new Player();
                            p.ShipName = reader.GetString(0);
                            p.GamerNo = reader.GetInt32(1);
                            fightList.Add(p);
                        }
                    }
                }
            }            
            return fightList;
        }
        public List<Player> CalucaleOutcome(List<Player> list)
        {
            Random rnd = new Random();
            // First one fire
            if (list[0].Ammo > 0)
            {
                list[0].Ammo -= list[0].Canons;
                list[1].Health -= rnd.Next((int)((list[0].Canons) / 2), list[0].Canons);

            }
            // Second one fire
            if (list[1].Ammo > 0)
            {
                list[1].Ammo -= list[0].Canons;
                list[0].Health -= rnd.Next((int)((list[0].Canons) / 2), list[1].Canons);
            }
            return list;
        } 
    }
}