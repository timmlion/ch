﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ClientObject : MonoBehaviour {

    public string str = "";

    public InputField login;
    public InputField password;

    private MainStatsControler MSC;
    private GameManagerScript GMS;
    private Gamer player;
    private List<Gamer> enemyList;
    private List<GameObject> enemyCanvas;

    private WebService1 ws;

    private void Awake()
    {
        MSC = gameObject.GetComponent<MainStatsControler>();
        GMS = gameObject.GetComponent<GameManagerScript>();
        GMS.AllDark();
        GMS.LoginCanvas.gameObject.SetActive(true);
        ws = new WebService1();
    }

    // Use this for initialization
    public void Login () {
        
        
        str = ws.HelloWorld();
        
        player = new Gamer();
        player = (Gamer)ws.Login(login.text.ToString(), password.text.ToString());
        
        Debug.Log(player.ShipName);

        if(player.GamerNo > 0)
        {
            GMS.AllDark();
            GMS.GameCanvas.gameObject.SetActive(true);
            ViewMainStats();

        }
        else
        {
            Debug.Log("Błędne dane logowania");
        }
        
	}
    public void Update()
    {
        
    }
    public void ViewMainStats()
    {
        MSC.ShipName.text = player.ShipName;
        MSC.Canons.text = "Canons: " + player.Canons.ToString();
        MSC.Crew.text = "Crew: " + player.Crew.ToString();
        MSC.Ammo.text = "Ammo: " + player.Ammo.ToString();
        MSC.Food.text = "Food: ";
        MSC.Gold.text = "Gold: " + player.Gold.ToString();
        MSC.Healt.text = "Health: " + player.Health.ToString();
    }
    public void LookAround()
    {
        GMS.AllDark();
        GMS.LookingCanvas.gameObject.SetActive(true);
        SetUpLooking();
    }
    public void GoBack()
    {
        GMS.AllDark();
        GMS.GameCanvas.gameObject.SetActive(true);
    }
    public void SetUpLooking()
    {
        GMS.EnemyCanvas1.gameObject.SetActive(false);
        GMS.EnemyCanvas2.gameObject.SetActive(false);

        var EnemyList = ws.LookAround(GPS.GpsInstance.lat, GPS.GpsInstance.lng);

        int i = 0;
        foreach(var enemy in EnemyList)
        {
            if (!(enemy.GamerNo == player.GamerNo)){

                Gamer p = new Gamer();
                p.GamerNo = enemy.GamerNo;
                p.ShipName = enemy.ShipName;
                // Wiem że brzydko... trudno jest 8:30, nie śpię od 26h ;P
                if (i == 0)
                {
                    GMS.EnemyCanvas1.GetComponentInChildren<Text>().text = enemy.ShipName;                    
                    GMS.EnemyCanvas1.gameObject.SetActive(true); 
                    
                }
                if (i == 1)
                {
                    GMS.EnemyCanvas2.GetComponentInChildren<Text>().text = enemy.ShipName;
                    GMS.EnemyCanvas2.gameObject.SetActive(true);
                }
                    i++;
            }
        }
    }

    public void Fight(int g2)
    {
        GMS.AllDark();
        GMS.FightCanvas.gameObject.SetActive(true);

        var fighters = ws.Fight(player.GamerNo, g2);

        MSC.f1ShipName.text = fighters[0].ShipName;
        MSC.f1Canons.text = "Canons: " + fighters[0].Canons.ToString();
        MSC.f1Crew.text = "Crew: " + fighters[0].Crew.ToString();
        MSC.f1Ammo.text = "Ammo: " + fighters[0].Ammo.ToString();
        MSC.f1Gold.text = "Gold: " + fighters[0].Gold.ToString();
        MSC.f1Healt.text = "Health: " + fighters[0].Health.ToString();

        MSC.f2ShipName.text = fighters[1].ShipName;
        MSC.f2Canons.text = "Canons: " + fighters[1].Canons.ToString();
        MSC.f2Crew.text = "Crew: " + fighters[1].Crew.ToString();
        MSC.f2Ammo.text = "Ammo: " + fighters[1].Ammo.ToString();
        MSC.f2Gold.text = "Gold: " + fighters[1].Gold.ToString();
        MSC.f2Healt.text = "Health: " + fighters[1].Health.ToString();
    }
    public void Fire(int g2)
    {

    }

}
