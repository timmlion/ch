﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPS : MonoBehaviour {

    public static GPS GpsInstance { get; set; }

    public float lat;
    public float lng;

    public bool isInUnityRemote = false;
    // Use this for initialization
	void Start () {


        GpsInstance = this;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
	}

    private IEnumerator StartLocationService()

    { 
        Debug.Log("start");
        GPSUpdate.text2static = "start";
        if (isInUnityRemote)
        {
            yield return new WaitForSeconds(5);
        }
        Debug.Log("Chceck");
        GPSUpdate.text2static = "check";
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("GPS not enabled");
            GPSUpdate.text2static = "GPS NOT ENABLED";
            yield break;
        }
        Input.location.Start();
        if (isInUnityRemote)
        {
            yield return new WaitForSeconds(5);
        }
        int maxWait = 20;
        while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            GPSUpdate.text2static = "wait " + maxWait;
            Debug.Log("Wait");
            yield return new WaitForSeconds(1);
            maxWait--;
            Debug.Log(maxWait);
        }

        if(maxWait <= 0)
        {
            GPSUpdate.text2static = "TIME OUT";
            Debug.Log("Time OUT");
            yield break;            
        }
        if(Input.location.status == LocationServiceStatus.Failed)
        {
            GPSUpdate.text2static = "NO LOCATION";
            Debug.Log("Unable to dermine location");
            yield break;
        }
        //lat = Input.location.lastData.latitude;
        //lng = Input.location.lastData.longitude;

        yield break;
    }

    // Update is called once per frame
    void Update () {
        lat = Input.location.lastData.latitude;
        lng = Input.location.lastData.longitude;
    }
}
