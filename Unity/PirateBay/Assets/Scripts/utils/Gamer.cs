﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Gamer : UnityEngine.MonoBehaviour
{
    public int GamerNo { get; set; }
    public int Crew { get; set; }
    public int Canons { get; set; }
    public int Ammo { get; set; }
    public int Gold { get; set; }
    public int Health { get; set; }
    public int Status { get; set; }
    public string ShipName { get; set; }

    public static explicit operator Gamer(Player v)
    {
        Gamer g = new Gamer();
        g.GamerNo = v.GamerNo;
        g.Crew = v.Crew;
        g.Canons = v.Canons;
        g.Ammo = v.Ammo;
        g.Gold = v.Gold;
        g.Health = v.Health;
        g.ShipName = v.ShipName;
        return g;
    }

}
