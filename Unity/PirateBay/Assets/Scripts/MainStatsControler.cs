﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainStatsControler : MonoBehaviour {

    public Text ShipName;
    public Text Canons;
    public Text Crew;
    public Text Ammo;
    public Text Food;
    public Text Gold;
    public Text Healt;

    public Text f1ShipName;
    public Text f1Canons;
    public Text f1Crew;
    public Text f1Ammo;
    public Text f1Gold;
    public Text f1Healt;

    public Text f2ShipName;
    public Text f2Canons;
    public Text f2Crew;
    public Text f2Ammo;
    public Text f2Gold;
    public Text f2Healt;
}
