﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {

    public Canvas LoginCanvas;
    public Canvas GameCanvas;
    public Canvas LookingCanvas;
    public Canvas AlertCanvas;
    public Canvas FightCanvas;

    public GameObject EnemyCanvas1;
    public GameObject EnemyCanvas2;

    public void AllDark()
    {
        LoginCanvas.gameObject.SetActive(false);
        GameCanvas.gameObject.SetActive(false);
        LookingCanvas.gameObject.SetActive(false);
        //AlertCanvas.gameObject.SetActive(false);
        FightCanvas.gameObject.SetActive(false);
    }

}
